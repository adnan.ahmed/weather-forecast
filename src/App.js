import React, { useState, useEffect, useRef } from 'react';
import './App.css';
import { BiChevronDown } from 'react-icons/bi';

function DropdownInput(props) {
  const [filteredItems, setFilteredItems] = useState([]);
  const [showDropdown, setShowDropdown] = useState(false);
  const dropdownRef = useRef(null);
  const [displayedItems, setDisplayedItem] = useState(0);


  useEffect(() => {
    const sortedItems = props.items.sort((a, b) => {
      if (!a.zip_code || !b.zip_code) {
        return 0;
      }
      return a.zip_code.toString().localeCompare(b.zip_code);
    });

    setFilteredItems(sortedItems);
  }, [props.items]);

  const handleInputChange = (event) => {
    const value = event.target.value.toLowerCase();

    const filtered = props.items.filter(item => {
      return item.city.toLowerCase().includes(value) || item.zip_code.toString().includes(value);
    });

    setFilteredItems(filtered);
  }

  const handleInputClick = () => {
    setShowDropdown(true);
  }

  const handleInputBlur = () => {
    setShowDropdown(false);
  }

  const handleScroll = () => {
    const dropdown = dropdownRef.current;
    if (dropdown && dropdown.scrollTop + dropdown.clientHeight === dropdown.scrollHeight) {
      setDisplayedItem(displayedItems + 50);
    }
  }

  return (
    <div className="dropdown" onClick={handleInputClick}>
      <div className="dropdown-input">
        <input className="input-field" type="text" id="input" placeholder={props.placeholder} onChange={handleInputChange} onBlur={handleInputBlur} />
        <span className="icon"><BiChevronDown /></span>
      </div>
      {showDropdown && (
        <ul className="dropdown-content show" ref={dropdownRef} onScroll={handleScroll}>
          {filteredItems.slice(0, displayedItems + 50).map((item, index) => (
            <li key={index}>{item.zip_code} ({item.city})</li>
          ))}
        </ul>
      )}
    </div>
  );
}

function App() {
  const [cities, setCities] = useState([]);

  useEffect(() => {
    fetch('https://raw.githubusercontent.com/millbj92/US-Zip-Codes-JSON/master/USCities.json')
      .then(response => response.json())
      .then(data => setCities(data));
  }, []);

  return (
    <div>
      <h1>Weather Forecast</h1>
      <DropdownInput placeholder="Zip Code" items={cities} />
    </div>
  );
}

export default App;
